﻿using Microsoft.EntityFrameworkCore;

namespace UserInfo.Models
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=UserInfoDb;Username=dotuser;Password=dotuser");
        }
    }
}