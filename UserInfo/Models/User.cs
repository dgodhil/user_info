﻿using UserInfo.Dto;

namespace UserInfo.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        public User() { }

        public User(int id, string firstName, string secondName, string lastName, int age)
        {
            this.Id = id;
            this.FirstName = firstName;
            this.SecondName = secondName;
            this.LastName = lastName;
            this.Age = age;
        }
        public User(int id, UserDto userDto)
        {
            this.Id = id;
            this.FirstName = userDto.FirstName;
            this.SecondName = userDto.SecondName;
            this.LastName = userDto.LastName;
            this.Age = userDto.Age;
        }

        public User(UserDto userDto)
        {
            this.FirstName = userDto.FirstName;
            this.SecondName = userDto.SecondName;
            this.LastName = userDto.LastName;
            this.Age = userDto.Age;
        }

    }
}