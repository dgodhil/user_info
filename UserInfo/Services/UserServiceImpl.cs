﻿using System.Collections.Generic;
using System.Linq;
using UserInfo.Dto;
using UserInfo.Fixtures;
using UserInfo.Models;

namespace UserInfo.Services
{
    public class UserServiceImpl : UserService
    {
        private static SortedDictionary<int, User> users;

        public UserServiceImpl()
        {
            if (users == null || !users.Any())
            {
                users = Fixture.getUserFixtures();
            }
        }

        public List<User> getAll()
        {
            return users.Values.ToList();
        }

        public User getById(int id)
        {
            return users.ContainsKey(id) ? users[id] : null;
        }

        public User create(UserDto userDto)
        {
            int id = users.Last().Key + 1;
            User user = new User(id, userDto.FirstName, userDto.SecondName, userDto.LastName, userDto.Age);
            users.Add(id, user);
            return user;
        }
        public User update(int id, UserDto userDto)
        {
            User user = users[id];
            updateUserInfo(user, userDto);
            return user;
        }

        public bool delete(int id)
        {
            if (users.ContainsKey(id))
            {
                users.Remove(id);
                return true;
            }
            return false;
        }

        public StatisticDto avgAge()
        {
            return new StatisticDto(users.Select(user => user.Value.Age).Average().ToString());
        }

        public StatisticDto usersCount()
        {
            return new StatisticDto(users.Count().ToString());
        }

        private void updateUserInfo(User user, UserDto userDto)
        {
            user.FirstName = userDto.FirstName;
            user.SecondName = userDto.SecondName;
            user.LastName = userDto.LastName;
            user.Age = userDto.Age;
        }
    }
}