﻿using System.Collections.Generic;
using UserInfo.Dto;
using UserInfo.Models;

namespace UserInfo.Services
{
    public interface UserService
    {
        List<User> getAll();
        User getById(int id);
        User create(UserDto userDto);
        User update(int id, UserDto userDto);
        bool delete(int id);
        StatisticDto avgAge();
        StatisticDto usersCount();
    }
}
