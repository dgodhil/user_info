﻿using System;
using System.Collections.Generic;
using UserInfo.Dto;
using UserInfo.Fixtures;
using UserInfo.Models;
using UserInfo.Repository;

namespace UserInfo.Services
{
    public class DbUserServiceImpl : UserService
    {
        private UserRepository userRepository;

        public DbUserServiceImpl(UserRepository userRepository)
        {
            this.userRepository = userRepository;
            loadFixtures();
        }

        public StatisticDto avgAge()
        {
            return new StatisticDto(userRepository.avgAge().ToString());
        }

        public User create(UserDto userDto)
        {
            User user = new User(userDto);
            userRepository.create(user);
            return user;
        }

        public bool delete(int id)
        {
            userRepository.delete(id);
            return true;
        }

        public List<User> getAll()
        {
            return userRepository.getAll();
        }

        public User getById(int id)
        {
            return userRepository.getById(id);
        }

        public User update(int id, UserDto userDto)
        {
            User user = userRepository.getById(id);
            updateUserInfo(user, userDto);
            userRepository.update(user);
            return user;
        }

        public StatisticDto usersCount()
        {
            return new StatisticDto(userRepository.usersCount().ToString());
        }

        private void updateUserInfo(User user, UserDto userDto)
        {
            user.FirstName = userDto.FirstName;
            user.SecondName = userDto.SecondName;
            user.LastName = userDto.LastName;
            user.Age = userDto.Age;
        }

        private void loadFixtures()
        {
            if (userRepository.usersCount() == 0)
            {
                new List<User>(Fixture.getUserFixtures().Values).ForEach(user => userRepository.create(user));
            }
        }
        
    }
}