﻿using System.Collections.Generic;
using UserInfo.Models;

namespace UserInfo.Fixtures
{
    public class Fixture
    {
        private static SortedDictionary<int, User> users;

        private Fixture() { }

        public static SortedDictionary<int, User> getUserFixtures()
        {
            users = new SortedDictionary<int, User>();

            users.Add(1, new User(1, "Иван", "Иванович", "Иванов", 36));
            users.Add(2, new User(2, "Петр", "Петрович", "Петров", 24));
            users.Add(3, new User(3, "Станислав", "Константинович", "Кошелев", 12));
            users.Add(4, new User(4, "Агафон", "Олегович", "Шаров", 68));
            users.Add(5, new User(5, "Маргарита", "Кимовна", "Веселова", 45));
            users.Add(6, new User(6, "Галина", "Тихоновна", "Турова", 22));
            users.Add(7, new User(7, "Борис", "Ефимович", "Воронов", 39));
            users.Add(8, new User(8, "Степан", "Геннадьевич", "Сорокин", 18));

            return users;
        }
    }
}