﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserInfo.Dto
{
    public class UserDto
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}