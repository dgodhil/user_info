﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserInfo.Dto
{
    public class StatisticDto
    {
        public string Value;

        public StatisticDto(string val)
        {
            this.Value = val;
        }
    }
}