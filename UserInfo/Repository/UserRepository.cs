﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserInfo.Models;

namespace UserInfo.Repository
{
    public interface UserRepository : IDisposable
    {
        List<User> getAll();
        User getById(int id);
        void create(User user);
        void update(User user);
        void delete(int id);
        double avgAge();
        int usersCount();
    }
}
