﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserInfo.Models;

namespace UserInfo.Repository
{
    public class UserRepositoryImpl : UserRepository, IDisposable
    {
        private ApplicationContext db;

        public UserRepositoryImpl(ApplicationContext context)
        {
            this.db = context;
        }

        public double avgAge()
        {
            return db.Users.Select(user => user.Age).Average();
        }

        public void create(User user)
        {
            db.Users.Add(user);
            db.SaveChanges();
        }

        public void delete(int id)
        {
            db.Users.Remove(db.Users.Find(id));
            db.SaveChanges();
        }

        public List<User> getAll()
        {
            return db.Users.OrderBy(user => user.Id).ToList();
        }

        public User getById(int id)
        {
            return db.Users.Find(id);
        }

        public void update(User user)
        {
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
        }

        public int usersCount()
        {
            return db.Users.Count();
        }

        //garbage collector
        private bool disposed = false;
        
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        //garbage collector end
    }
}