﻿using System.Collections.Generic;
using System.Web.Http;
using UserInfo.Dto;
using UserInfo.Models;
using UserInfo.Services;

namespace UserInfo.Controllers
{
    public class UsersController : ApiController
    {
        private UserService userService;

        public UsersController(UserService userService)
        {
            this.userService = userService;
        }


        public IEnumerable<User> Get()
        {
            return userService.getAll();
        }

        public User Get(int id)
        {
            return userService.getById(id);
        }

        public User Post([FromBody]UserDto user)
        {
            return userService.create(user);
        }

        public User Put(int id, [FromBody]UserDto user)
        {
            return userService.update(id, user);
        }

        public bool Delete(int id)
        {
            return userService.delete(id);
        }
        
        [HttpGet]
        [Route("api/Users/count")]
        public StatisticDto usersCount()
        {
            return userService.usersCount();
        }


        [HttpGet]
        [Route("api/Users/avgAge")]
        public StatisticDto avgAge()
        {
            return userService.avgAge();
        }

    }
}
