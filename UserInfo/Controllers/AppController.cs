﻿using System.Web.Mvc;

namespace UserInfo.Controllers
{
    public class AppController : Controller
    {
        public ActionResult Statistic()
        {
            ViewBag.Title = "Статистика";
            return View();
        }
        
        public ActionResult Information()
        {
            ViewBag.Title = "Пользователи";
            return View();
        }
    }
}